import { LightningElement, api, wire, track } from 'lwc';
import getSimilarItems from '@salesforce/apex/SimilarItemsController.getSimilarItems';
import { NavigationMixin } from 'lightning/navigation';

export default class SimilarItems extends NavigationMixin(LightningElement) {
    @api recordId;
    @api objectApiName;
    @track similarProjects;
    @track errorMsg;
    
    @wire(getSimilarItems, {recordId: '$recordId'}) 
    wiredsimilarProjects (value){
        this.wiredRecords = value;
        if(value.error){
            this.errorMsg= value.error;
        }else if(value.data){
            this.similarProjects = value.data;
        }
    }
    
    //navigate to record detail page
    handleRecordView(event){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                actionName: "view",
                recordId: event.target.value,
                objectApiName: "Cable_Project__c"
            }
        });
    }

    //Edit record
    editRecord(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.target.value,
                objectApiName: 'Cable_Project__c',
                actionName: 'edit',
            }
        });
    }
     
}