public with sharing class SimilarItemsController {

    //retrive similar Project Records
    @AuraEnabled(cacheable=true)
    public static List<sObject> getSimilarItems(string recordId){

        // Validate parameters
        if(recordId==null || recordId=='')
            throw new similarItemsException('Record Id not specified.');

        Map<String, Cable_Project__c> mapOfRecords = new Map<String, Cable_Project__c>();

        for(Cable_Project__c tempVariable: [SELECT Id, Network_Platform_formula__c, Network_Platform__c, Cable_Genre__c FROM Cable_Project__c WHERE Id= :recordId]){
            mapOfRecords.put(tempVariable.Id, tempVariable);
        }

        if(mapOfRecords.size()>0){
            List<Cable_Project__c> similarRecords = [SELECT Id, Name, Network_Platform_formula__c, Cable_Genre__c
                                                    FROM Cable_Project__c 
                                                    WHERE Network_Platform__c=: mapOfRecords.get(recordId).Network_Platform__c 
                                                    AND Cable_Genre__c =: mapOfRecords.get(recordId).Cable_Genre__c];
        
            System.debug(similarRecords.size());
            if(similarRecords.size()>0)
        return similarRecords;
        }
        System.debug('invalid record Id: '+recordId+ '--->'+mapOfRecords.size());
        return null;
    }

    public class similarItemsException extends Exception {}

}
